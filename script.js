function toUpper(char) {
    // ASCII код букв a-z: 97-122, A-Z: 65-90
    //принимает символ в качестве аргумента и возвращает либо аналогичный символ в верхнем регистре,
    //либо оставляет его без изменений, если это не буква в нижнем регистр
    if (char.charCodeAt(0) >= 97 && char.charCodeAt(0) <= 122) {
      return String.fromCharCode(char.charCodeAt(0) - 32);
    }
    else {
      return char;
    }
  }

  function convert() {
    const input = document.getElementById("input").value;
    const output = toUpper(input);
    document.getElementById("output").innerHTML = output;
  }